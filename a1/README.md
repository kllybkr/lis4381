> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Kelly Baker

### Assignment #1 Requirements:

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter questions (1-2)


#### README.md file should include the following items:

* Screenshot of AMPPS installation
* Screenshot of running Java Hello
* Screenshot of running Android Studio - My First App
* git commands with short descriptions
* Bitbucket repo links

> #### Git commands w/short descriptions:

1. git init - creates new Git repository
2. git status - shows the state of the directory
3. git add - adds a change  in the directory to the staging area
4. git commit - records changes to the repository
5. git push - updates remote repository
6. git pull - downloads changes from remote to local repository
7. git branch - creates, updates, edits branch

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps_ss.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/terminal_ss.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android_ss.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/kllybkr/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/kllybkr/myteamquotes/ "My Team Quotes Tutorial")
